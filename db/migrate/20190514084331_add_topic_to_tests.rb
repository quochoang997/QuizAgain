class AddTopicToTests < ActiveRecord::Migration[5.2]
  def change
    add_reference :tests, :topic, foreign_key: true
  end
end
