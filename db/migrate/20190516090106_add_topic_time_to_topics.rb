class AddTopicTimeToTopics < ActiveRecord::Migration[5.2]
  def change
    add_column :topics, :topic_time, :integer, default: 420
  end
end
