class CreateTests < ActiveRecord::Migration[5.2]
  def change
    create_table :tests do |t|
      t.string :id_ques_list
      t.string :answer_list
      t.integer :score
      t.integer :test_time
      t.belongs_to :user, foreign_key: true

      t.timestamps
    end
  end
end
