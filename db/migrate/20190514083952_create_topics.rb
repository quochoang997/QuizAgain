class CreateTopics < ActiveRecord::Migration[5.2]
  def change
    create_table :topics do |t|
      t.string :title, null: false
      t.boolean :disable, default: false
      t.integer :number_of_question, default: 0

      t.timestamps
    end
  end
end
