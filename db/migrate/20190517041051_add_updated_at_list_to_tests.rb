class AddUpdatedAtListToTests < ActiveRecord::Migration[5.2]
  def change
    add_column :tests, :updated_at_list, :string, default: ""
  end
end