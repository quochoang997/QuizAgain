class CreateQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :questions do |t|
      t.text :content, null: false 
      t.string :attach_type
      t.string :question_type
      t.boolean :disable, default: false

      t.timestamps
    end
  end
end
