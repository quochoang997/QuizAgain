Rails.application.routes.draw do
  get 'errors/not_found'
  get 'errors/internal_server_error'
  get 'home/index'
  get 'topics/index'
  root 'home#index'
  get 'home/test'
  devise_for :users
  resources :questions #, only: [:new, :create, :edit, :update]
  resources :tests ,only: [:index, :show]
  resources :users ,only: [:index]
  resources :topics

  root to: 'questions#index'
  #get 'do_test', to: 'tests#do_test' #, as: 'tests_play_by_ques'
  get '/do_test/:id', to: 'tests#do_test', as: 'do_test'
  post '/do_test/:id', to: 'tests#show_result'

  patch  'setStatus'  => 'questions#setStatus'
  post  'create_test' => 'tests#create_test'
  patch  'setRole'  => 'users#setRole'
  patch 'setStatusTopic' => 'topics#setStatusTopic'

  get 'all_question' => 'questions#all_question'
  get 'enable_question' => 'questions#enable_question'
  get 'disable_question' => 'questions#disable_question'

  match "/404", :to => "errors#not_found", :via => :all , as: 'not_found'
  match "/500", :to => "errors#internal_server_error", :via => :all , as: 'internal_server_error'
end
