class TopicsController < ApplicationController
  before_action :only_admin

  def index
    @topics = Topic.search(params[:topic_search]).paginate(page: params[:page], per_page: 6)
    @number_all = Topic.count
    @number_enable = Topic.count{|topic| topic.disable == false}
    @number_disable = @number_all - @number_enable
    session[:present_tab] = params[:topic_search]
  end

  def create
    @topic = Topic.new(create_topic_params)

    respond_to do |format|
      if @topic.save
        @number_all = Topic.count
        @number_enable = Topic.count{|topic| topic.disable == false}
        format.html {redirect_to topics_path}
        format.js {}
      else
        flash[:danger] = @topic.errors.full_messages.first
        format.html {redirect_to topics_path}
      end
    end
  end

  def show
    @topic = Topic.find_by(id: params[:id])
    @questions = @topic.questions.paginate(page: params[:page], per_page: 6)
  end

  def edit
    @topic = Topic.find_by(id: params[:id])
  end

  def update
    @topic = Topic.find_by(id: params[:id])
    p 2,3,3,4,5,params

    respond_to do |format|
      if @topic.update(update_topic_params)
        format.html {redirect_to topics_path}
        format.js {}
      else
        flash[:danger] = @topic.errors.full_messages.first
        format.html {redirect_to topics_path}
      end
    end
  end

  def setStatusTopic
    @topic = Topic.find_by(id: params[:id])
    if @topic.disable == true
      @topic.update(disable: false)
      @topic_status = 'Công khai'
      @b = "Disable"
    else
      @topic.update(disable: true)
      @topic_status = 'Vô hiệu hóa'
      @b = "Enable"
    end

    @number_enable = Topic.count{|topic| topic.disable == false}
    @number_disable = Topic.count{|topic| topic.disable == true}

    respond_to do |format|
      format.html {redirect_to topics_path}
      format.js {}
    end
	end

  def create_topic_params
    params.require(:topic).permit(:title , :number_of_question ,:topic_time)
  end

  def update_topic_params
    params[:topic][:topic_time] = params[:topic][:topic_time].to_i * 60
    params.require(:topic).permit(:title, :disable, :number_of_question ,:topic_time )
  end

  def setStatus
    @topic = Topic.find_by(id: params[:id])
    @topic.update(disable: !(@topic.disable))
    @b = @topic.disable.to_s
  end
end
