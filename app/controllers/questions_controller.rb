class QuestionsController < ApplicationController
  before_action :only_admin

	def index
    @questions = Question.search(params[:content_search], params[:status_search]).paginate(page: params[:page], per_page: 6)
    session[:all] = params[:status_search]
    @total_number_of_question = Question.count
    @number_of_enable_question = Question.count {|question| question.disable == false}
    @number_of_disable_question = Question.count {|question| question.disable == true}
  end

  def new
    @question = Question.new(attach_type: "no_attach")
    @topics = Topic.all
    @question.answers.build
    @question.answers.build
  end

  def create
    @question = Question.new(question_params)
    # @question.topic = Topic.find_by(id: params[:question][:topic])

    case @question.attach_type
    when "image"
      @question.image.attach(params[:question][:image])
    when "sound"
      @question.sound.attach(params[:question][:sound])
    when "video"
      @question.video.attach(params[:question][:video])
    end

    if @question.save
      flash[:success] = "create successfully"
      redirect_to @question
    else
      flash[:danger] = @question.errors.full_messages.first
		  render :new
		end
  end

	def edit
    @question = Question.find_by(id: params[:id])
    @topics = Topic.all
	end

	def update
	  @question = Question.find_by(id: params[:id])
	  if @question.update_attributes(question_params)
	    redirect_to root_path
	  else
	    render :edit
	  end
  end

  def show
    @question = Question.find_by(id: params[:id])
  end

	def all_question
    @questions = Question.all
    session[:all] = 'all'
    respond_to do |format|
      format.html {redirect_to questions_path}
      format.js {}
    end
  end

  def enable_question
    @questions = Question.select {|question| question.disable == false}
    session[:all] = 'enable'
    respond_to do |format|
      format.html {redirect_to questions_path}
      format.js {}
    end
  end

  def disable_question
    @questions = Question.select {|question| question.disable == true}
    session[:all] = 'disable'
    respond_to do |format|
      format.html {redirect_to questions_path}
      format.js {}
    end
	end
	
  def setStatus
    @question = Question.find(params[:id])
    if @question.disable == true
      @question.update(disable: false)
      @status = 'Công khai'
      @b = 'Disable'
    else
      @question.update(disable: true)
      @status = 'Vô hiệu hóa'
      @b = 'Enable'
    end

    @number_of_enable_question = Question.count {|question| question.disable == false}
    @number_of_disable_question = Question.count {|question| question.disable == true}

    respond_to do |format|
      format.html {redirect_to questions_path}
      format.js {}
    end
  end
  
  def destroy
    @question = Question.find_by(id: params[:id])
    respond_to do |format|
      if @question.disable == false
        flash[:danger] = "Cannot delete enable question"
        format.html {redirect_to questions_path}
        # format.js {}
      else
        if @question.destroy
          flash[:success] =  'Question was successfully destroyed.' 
          format.html { redirect_to questions_path}
          format.js   {}
        else
          flash[:danger] = 'Destroy question failed.'
          format.html { redirect_to questions_path}
          format.js   {}
        end
      end
    end
  end
	
  private
	def question_params
	  params.require(:question).permit(:content, :attach_type, :question_type, :disable, :topic_id ,
	  	answers_attributes: [:id, :content, :correct, :_destroy])
	end
end
