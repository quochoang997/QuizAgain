class TestsController < ApplicationController
  def index
    if current_user.role == 'admin'    
      @tests = Test.all.order("id DESC").paginate(page: params[:page], per_page: 6)
    elsif 
      @tests = current_user.tests.all.order("id DESC").paginate(page: params[:page], per_page: 6)
    end
  end
  
  def do_test
    topic = Topic.find_by(id: params[:id])
    if !topic or topic.number_of_question > topic.questions.size or topic.disable == true
      redirect_to not_found_path
    else
      id_ques_list = createTest(params[:id])

      @test = current_user.tests.new(id_ques_list: id_ques_list , topic_id: params[:id])
    end
  end

  def show
    if current_user.role == 'admin'
      @test = Test.find_by(id: params[:id])
    else
      @test = current_user.tests.find_by(id: params[:id])
    end
    if !@test 
      redirect_to not_found_path
    end
  end

  def show_result
    test = current_user.tests.find_by(id_ques_list: params[:answers][:id_ques_list])
    if test
      @test = test
    else
      @test = current_user.tests.new(id_ques_list: params[:answers][:id_ques_list] , topic_id: 1)
      @score = 0
      @answer_list = ""
      @test.id_ques_list.split('/').each do |id|
        isTrue = true
        @question = Question.find(id)
        @question.answers.each_with_index do |answer , index|
          answer_result = params[:answers]["#{id}"]["#{index}"]
          check_answer_result = check_answer answer_result, answer.correct
          @answer_list += check_answer_result + ","
          if check_answer_result == '0' or check_answer_result == '1'
            isTrue = false
          end
        end
        if isTrue == true
          @score += 1
        end
        @answer_list = @answer_list.first(-1) + '/'
      end  
      @answer_list = @answer_list.first(-1)
      @test.answer_list = @answer_list
      @test.score = @score
      @test.test_time = params[:answers][:time_do_test].to_i
      @test.updated_at_list = params[:answers][:updated_at_list]
      @test.save
    end
  end

  def check_answer aws_check, aws_correct
    if aws_correct == true
      if aws_check == "0"
        return '1'
      else
        return '2'
      end
    else
      if aws_check == "0"
        return '3'
      else
        return '0'
      end
    end
  end

  #code for create test
  def createTest(topic_id)
    @topic = Topic.find_by(id: topic_id)
    number_of_question_per_test = @topic.number_of_question
    _test = Array.new
    if number_of_question_per_test < 5
      @questions = @topic.questions.where(['disable = ?', false])
      random(_test, @questions, number_of_question_per_test)
    else
      @questions = @topic.questions.where(['disable = ?', false]) #select all enable question
      number_of_enable_question = @questions.count
      number_of_easy_question = @questions.where(difficulty: "easy").count
      number_of_normal_question = @questions.where(difficulty: "normal").count
      number_of_hard_question = @questions.where(difficulty: "hard").count
      _easy, _normal, _hard = number_of_question_of_each_difficulty(number_of_easy_question, number_of_normal_question, number_of_hard_question, number_of_question_per_test)
      random(_test, @questions.where(difficulty: "easy"), _easy)
      random(_test, @questions.where(difficulty: "normal"), _normal)
      random(_test, @questions.where(difficulty: "hard"), _hard)
    end
    _test.shuffle!
    return _test.join("/")
  end

  private
  def random(test, questions, num)
    num.times do
      a = true
      begin
        question = questions.sample
        unless test.include?(question.id)
          a = false
          test.push(question.id)
        end
      end while a
    end
  end

  private
  def number_of_question_of_each_difficulty(easy, normal, hard, number_of_question)
    _easy = div_round_up(number_of_question)
  	_normal = _easy
  	_hard = number_of_question - 2*_easy
  	_can_easy = true
  	_can_normal = true
  	_can_hard = true
  	_odd = 0

  	begin
	  	if easy <= _easy
		  	_odd += _easy - easy
		  	_easy = easy
		  	_can_easy = false
		  end

		  if normal <= _normal
		  	_odd += _normal - normal
		  	_normal = normal
		  	_can_normal = false
		  end

		  if hard <= _hard
		  	_odd += _hard - hard
		  	_hard = hard
		  	_can_hard = false
		  end

		  if _can_easy == true
		  	_easy += _odd
		  	_odd = 0
		  elsif _can_normal == true
		  	_normal += _odd
		  	_odd = 0
		  elsif _can_hard == true
		  	_hard += _odd
		  	_odd = 0
		  end

	  end until _odd == 0
	  return _easy, _normal, _hard
  end

  private
  def div_round_up(a)
	  if a % 3 == 0
		  return a/3
	  else
		  return a/3 + 1
	  end
  end
end
