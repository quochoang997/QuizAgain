class HomeController < ApplicationController
  def index
    @topics = Topic.where(disable: false)
    @topics.each do |value|
      if value.number_of_question > value.questions.size
        @topics = @topics.reject{|x| x ==value}
      end
    end
  end

  def test
  end
end
