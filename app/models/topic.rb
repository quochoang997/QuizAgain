class Topic < ApplicationRecord
  has_many :questions
  has_many :tests

  validates :title, presence: true,
					length: { minimum: 5, maximun: 1000 }
					
	# validate :number_of_question, 
          
  def self.search(topic_search)
    case topic_search
		when 'all'
			all
		when 'enable'
			where(['disable = ?', false])
		when 'disable'
			where(['disable = ?', true])
		else
			all
		end
  end
end
