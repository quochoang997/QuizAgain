$(document).ready(function(){ 
  $("#add-answer , .remove_fields").click(function(event) {
    numChecked = $("#answers .nested-fields").length;
    console.log("answ : " + numChecked);
  });

  $( "#disable-ques-link" ).click(function() {
    $( "#status_search").val("disable");
  });
  $( "#all-ques-link" ).click(function() {
    $( "#status_search").val("all");
  });
  $( "#enable-ques-link" ).click(function() {
    $( "#status_search").val("enable");
  });

  $(".new_question").attr('novalidate', true);
  $(".edit_question").attr('novalidate', true);
  
  $(".dropdown-menu li").click(function(){
    var select_acc = $(this).val();
    if(select_acc == 0){
      $("#attach_type").val("no_attach"); 
      $(this).parents(".btn-group").find('.selection').text($(this).text());
    }
  });


  $("#image-upload").change(function(event){
    $(".selection").text("");
    $("#index-upload").text("1");
    $(".selection").append("<i class='fa fa-picture-o' style='font-size: 25px;'></i> Hình ảnh");
    var preview = $(".image-preview img");
    preview.show();
    $('#video_player').hide();
    $('#audio_player').hide();

    if (!image_validation()){
      return
    }

    var input = $(event.currentTarget);
    var file = input[0].files[0];
    var reader = new FileReader();
    reader.onload = function(e){
        image_base64 = e.target.result;
        preview.attr("src", image_base64);
    };
    reader.readAsDataURL(file);
    $("#attach_type").val("image"); 
  });

  $("#audio-upload").change(function(event){
    $(".selection").text("");
    $("#index-upload").text("1");
    $(".selection").append('<i class="fa fa-file-audio-o" style="font-size: 25px;"></i> Audio ngắn');
    $("#attach_type").val("sound"); 
    var audio = $("#audio_player");
    audio.show();
    $('#video_player').hide();
    $('.image-preview img').hide();

    if (!audio_validation()){
      return
    }

    audio[0].src = URL.createObjectURL(this.files[0]);
    audio[0].load();
    audio[0].play();
  });

  $("#video-upload").change(function(event){
    $(".selection").text("");
    $("#index-upload").text("1");
    $(".selection").append('<i class="fa fa-file-video-o" style="font-size: 25px;"></i> Video ngắn');
    $("#attach_type").val("video"); 
    var video = $("#video_player");
    video.show();
    $('#audio_player').hide();
    $('.image-preview img').hide();

    if (!video_validation()){
      return
    }

    video[0].src = URL.createObjectURL(this.files[0]);
    video[0].load();
    video[0].play();
  });

  $(".email-action li").click(function(){
    $(".list-group-item").css("background-color", "white");
    $(this).css("background-color", "gainsboro");
  });

  // script for question search
  $("#question-search input").on('input', function() {
      $.get($("#question-search").attr("action"), $("#question-search").serialize(), null, "script");
      return false;
  });

  $("#question-search select").change(function(){
      $.get($("#question-search").attr("action"), $("#question-search").serialize(), null, "script");
      return false;
  });


  // Validate form 
  $("#submit-question-btn").click(function(event) {
    var fileSize = 0;
    var isSubmit = true;
    var inputName;

    var attach_type = $("#attach_type").val();
    if(attach_type == "image"){
      inputName = "#image-upload";
    }
    else if(attach_type == "no_attach"){
      inputName = "#image-upload";
    }
    else if(attach_type == "sound"){
      inputName = "#audio-upload";
    }
    else{
      inputName = "#video-upload";
    }
    if($(inputName)[0].files[0]){
      fileSize = $(inputName)[0].files[0].size;
    }
    
    var filename = $(inputName).val();
    var extension = filename.replace(/^.*\./, '');
    extension = extension.toLowerCase();
    if(attach_type == "image"){
      if(!(extension == 'jpg' || extension == 'jpeg' || extension == 'png') || fileSize > 5242880 ){
        $("#invalid-file").text("Định dạng file hình ảnh không hợp lệ hoặc dung lượng quá 5MB");
        isSubmit = false;
      }
    }
    else if(attach_type == "no_attach"){
    }
    else if(attach_type == "sound"){
      if(extension != 'mp3' || fileSize > 10485760 ){
        $("#invalid-file").text("Định dạng file âm thanh không hợp lệ hoặc dung lượng quá 10MB");
        isSubmit = false;
      }
    }
    else{
      if(extension != 'mp4' || fileSize > 104857600 ){
        $("#invalid-file").text("Định dạng file video không hợp lệ hoặc dung lượng quá 100MB");
        isSubmit = false;
      }
    }

    console.log("Name input :" + inputName);
    console.log("attach_type : " + $("#attach_type").val());
    console.log("fileSize : " + fileSize);
    console.log("extension : " + extension);
    if(isSubmit == false){
      $("#invalid-file").css("visibility", "visible");
      event.preventDefault();
    }
    else{
      $("#invalid-file").css("visibility", "hidden");
    }
  });

});


function content_validation(){
  x = $("#question_content").val().toString();
  if (x.length < 5){
    $("#content-message").html("Question must be more than 5 characters");
    return false;
  } else if (x.length > 1000){
    $("#content-message").html("Question must be less than 1000 characters");
    return false;
  } else {
    $("#content-message").html("");
    return true;
  }
}
function image_validation(){
  var input = $("#image-upload");
  var file = input[0].files[0];

  var type = file.type
  var size = file.size;
  if (!type.startsWith("image/")){
    $("#attach-file-message").html("This file is not image");
    $("#image-upload").val('');
    $(".image-preview img").attr("src", "");
    return false
  } else if (size > 5242880){
    $("#attach-file-message").html("Image size must be less than 5 MB");
    $("#image-upload").val('');
    $(".image-preview img").attr("src", "");
    return false
  } else {
    $("#attach-file-message").html("");
  }
  return true
}
function video_validation(){
  var input = $("#video-upload");
  var file = input[0].files[0];
  var type = file.type;
  var size = file.size;
      
  if (!type.startsWith("video/")){
    $("#attach-file-message").html("This file is not video");
    $("#video-upload").val('');
    $("#video_player").attr("src", "");
   return false
  } else if (size > 104857600){
    $("#attach-file-message").html("Video size must be less than 100 MB");
    $("#video-upload").val('');
    $("#video_player").attr("src", "");
    return false
  } else {
    $("#attach-file-message").html("");
  }
  return true
}
function audio_validation(){
  var input = $("#audio-upload");
  var file = input[0].files[0];
  var type = file.type;
  var size = file.size;
      
  if (!type.startsWith("audio/")){
    $("#attach-file-message").html("This file is not audio");
    $("#audio-upload").val('');
    $("#audio_player").attr("src", "");
   return false
  } else if (size > 10485760){
    $("#attach-file-message").html("Audio size must be less than 10 MB");
    $("#audio-upload").val('');
    $("#audio_player").attr("src", "");
    return false
  } else {
    $("#attach-file-message").html("");
  }
  return true
}
function answer_validation(event){
  answer_check(event.data.answer_field_id, event.data.answer_message_id);
}
function answer_validation_2(){
  var answer1 = answer_check("#question_answer1", "#answer1-message");
  var answer2 = answer_check("#question_answer2", "#answer2-message");
  var answer3 = answer_check("#question_answer3", "#answer3-message");
  var answer4 = answer_check("#question_answer4", "#answer4-message");
  return answer1 && answer2 && answer3 && answer4
}
function answer_check(answer_id, correct_id){
  var answer = $(answer_id).val().toString();
  if (answer.length < 3){
    $(correct_id).html("Answer must be more than 3 characters");
    return false
  } else if (answer.length > 100){
    $(correct_id).html("Answer must be less than 100 characters");
    return false
  } else {
    $(correct_id).html("");
    return true
  }
}
function checkbox_validation(){
  var correct1 = $("#question_correct1").prop('checked');
  var correct2 = $("#question_correct2").prop('checked');
  var correct3 = $("#question_correct3").prop('checked');
  var correct4 = $("#question_correct4").prop('checked');
  if (correct1 || correct2 || correct3 || correct4){
    $("#checkbox-message").html("");
    return true;
  } else {
    $("#checkbox-message").html("One checkbox must be checked");
    return false;
  }
}

function display_frame(){
  var attach_type = $("#attach_type").val();
  
  if(attach_type == "image"){
    $(".btn-group").find('.selection').text('Hình ảnh');
  }
  else if(attach_type == "no_attach"){
    $(".btn-group").find('.selection').text('Không');
  }
  else if(attach_type == "sound"){
    $(".btn-group").find('.selection').text('Audio ngắn');
  }
  else{
    $(".btn-group").find('.selection').text('Video ngắn');
  }
  
}
(function() {
	'use strict';
	window.addEventListener('load', function() {
		// Fetch all the forms we want to apply custom Bootstrap validation styles to
		var forms = document.getElementsByClassName('new_question');
		var content = document.getElementById('ques-content');

		
		// Loop over them and prevent submission
		var validation = Array.prototype.filter.call(forms, function(form) {
			form.addEventListener('submit', function(event) {
				if (content != null) {
					console.log(content.value.length);
				}

				if (form.checkValidity() === false) {
					event.preventDefault();
					event.stopPropagation();
				}
				form.classList.add('was-validated');
			}, false);
		});
	}, false);
})();

(function() {
	'use strict';
	window.addEventListener('load', function() {
		// Fetch all the forms we want to apply custom Bootstrap validation styles to
		var forms = document.getElementsByClassName('edit_question');
		var content = document.getElementById('ques-content');

		
		// Loop over them and prevent submission
		var validation = Array.prototype.filter.call(forms, function(form) {
			form.addEventListener('submit', function(event) {
				if (content != null) {
					console.log(content.value.length);
				}

				if (form.checkValidity() === false) {
					event.preventDefault();
					event.stopPropagation();
				}
				form.classList.add('was-validated');
			}, false);
		});
	}, false);
})();

function auto_grow(element) {
	element.style.height = "5px";
	element.style.height = (element.scrollHeight)+"px";
}