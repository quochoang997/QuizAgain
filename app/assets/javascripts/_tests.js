$(document).ready(function() {
  $(".question-selection").click(function(){
    var parent = $(this).parent().parent().parent();
    if(parent.css("background-color") == "rgb(255, 230, 234)"){
      parent.css("background-color", "white");
      parent.find('.invalid-feedback').css("visibility", "hidden");
    }
    var checked = $(this).find("input").prop('checked') ;
    if(checked == false){
      $(this).css("background-color", "rgba(56, 7, 80, 0.91)");
      $(this).find("input").prop('checked', true);
    }
    else{
      $(this).css("background-color", "#f8f8f8");
      $(this).find("input").prop('checked', false);
    }
  });

  $("#do-test-submit").click(function(event) {
    var i = 0;
    var numChecked ;
    var isFalse = true ;
    var firstId = 0;
		var numItems = $('.question-block').length;
    for (i; i < numItems; i++) { 
      numChecked = $(".question-index-"+i.toString()+" input:checked").length;
      if(numChecked == 0){
        if(isFalse){
          firstId = i;
          isFalse = false;
        }
        
        $(".question-index-"+i.toString()).css("background-color", "#FFE6EA");
				$(".invalid-index-"+i.toString()).css("visibility", "visible");
        event.preventDefault();
        
      }
    }
    if(isFalse == false){
      $(window).scrollTop($('#question-index-'+firstId+'-id').offset().top);
    }
    
	});

  // script for test search
	$("#test_search").on('input', function() {
		$.get($("#test-search").attr("action"), $("#test-search").serialize(), null, "script");
		return false;
	});

});

function CountDown(duration) {
  if (!isNaN(duration)) {
    var timer = duration, minutes, seconds;
      
    var interVal=  setInterval(function () {
      minutes = parseInt(timer / 60, 10);
      seconds = parseInt(timer % 60, 10);
      minutes = minutes < 10 ? "0" + minutes : minutes;
      seconds = seconds < 10 ? "0" + seconds : seconds;
      console.log(duration - timer);
      $('#time_do_test').val(duration - timer);
      $('#minute').html(minutes);
      $('#second').html(seconds);
      if (--timer < 0) {
        timer = duration;
        clearInterval(interVal);
        alert("Đã quá thời gian làm bài");
        $('form').submit();
      }
      },1000);
  }
}